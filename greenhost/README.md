# Ansible Collection - guardianproject.greenhost

This collection installs two modules (ansible_vm and ansible_ssh) and one inventory plugin (greenhost). This collection depends on Greenhost's cloud API client from https://open.greenhost.net/greenhost/cloud-api. Environment variable `COSMOS_API_TOKEN` must be set for the collection modules to work.
```
pip install https://open.greenhost.net/greenhost/cloud-api/-/archive/master/cloud-api-master.zip
export COSMOS_API_TOKEN=xxxxxxxxxxxxxxxxxxxx
```

To install this collection, point ansible-galaxy at this repo:

```
ansible-galaxy collection install git+https://gitlab.com/guardianproject-ops/ansible-greenhost
```

### ansible_ssh
Example SSH key creation:

```
    - name: create ssh key
      guardianproject.greenhost.greenhost_ssh:
        name: test-key
        public_key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQD1DUk8QoUnbEJGRlM84NEbwEqbDr3WP5hT8iH+UP0Cxq26ZyOQco9+tadBy9M33wvZ8Ij7R5Cppewp08LID3S57UkT14pCZxG6d38PaPEk8IQuDL9BkXSYrZWs1nPzexvs8yVwNnREcY+YPq63AYlbdm5WYbu2QsbXAHcyDX9sCU1mFvH37Mj9phCJLmdvLPvS9A8Qm0eOUE6wOYgikovhQ55bmWto09t8ejCOJqyeHr9HspQMLidRGojttVHsEHVyb+3FIRsWb8nugxkLcWXcPIaN0vhDC6GvoHavg4wSiI5i1WVu8q6R00GoBPQtv4IVVmQpqP+uIC/atLpqCbVjca659yFfQcvtHgi90JDiWcrrf4LF/zj/Q06HN14p+ofO8Pq/ve9pdyKFJJVWhfSRnht0QZrLku6UFK7x4cz26HPCyuOElBQwpqLULPVzNrfO6CAi7DEd07cSZWJpx/wgl8WRdFsO0qNGJwd+u5C2Nx/QwR8S7vSE9AxxzEuD/kc= ana@firebrick"
        state: present
      register: out

    - name: dump output
      debug:
        msg: "{{ out }}"
```

Example SSH key removal:

```
    - name: remove ssh key
      guardianproject.greenhost.greenhost_ssh:
        name: test-key
        state: absent
      register: out

    - name: dump output
      debug:
        msg: "{{ out }}"
```
### ansible_vm

Example VM creation:

```
- name: create vm
  guardianproject.greenhost.greenhost_vm:
    name: test-vm
    ssh_key: test-key
    state: present
register: out

- name: dump output
    debug:
      msg: "{{ out }}"
 ```
Example VM removal:

```
- name: remove vm
  guardianproject.greenhost.greenhost_vm:
    name: test-vm
    ssh_key: test-key
    state: absent
register: out

- name: dump output
    debug:
      msg: "{{ out }}"
 ```

### greenhost inventory plugin

The plugin discovers all existing VMs in a Greenhost account, provided environment variable `COSMOS_API_TOKEN` is defined.
Include the inventory plugin name in `ansible.cfg`:

```
[inventory]
enable_plugins=guardianproject.greenhost.greenhost
```
