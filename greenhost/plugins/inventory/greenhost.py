from ansible.plugins.inventory import BaseInventoryPlugin
from ansible.module_utils.common.text.converters import to_native
from greenhost_cloud import cosmos


class InventoryModule(BaseInventoryPlugin):
    NAME = 'greenhost-plugin'

    def verify_file(self, path):
        # there is nothing to verify, we don't expect a class or string, instead we use the greenhost API
        # this function must be present and return True otherwise nothing is parsed
        valid = True
        return valid

    def parse(self, inventory, loader, path, cache=False):
        super(InventoryModule, self).parse(inventory, loader, path, cache)
        try:
            all_vms = cosmos.request_api(resource="droplets")
            for vm in all_vms["droplets"]:
                self.inventory.add_host(vm["name"])
                for network in vm["networks"]["v4"]:
                    self.inventory.set_variable(vm["name"], "ansible_host",
                                                network["ip_address"])
        except Exception as e:
            raise AnsibleParserError(
                f"Encountered error, original message was {to_native(e)}")
