#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
import json
from greenhost_cloud import cosmos

DOCUMENTATION = r'''
---
module: greenhost_ssh

short_description: Automate the creation and deletion of SSH keys in Greenhost

version_added: "1.0.0"

description: This module relies on the greenhost cloud api to create and delete 
SSH keys for automation purposes.
The Greenhost API token must be exported in the environment variable COSMOS_API_TOKEN.

options:
    name:
        description: This is the name of the SSH key to create or delete
        required: True
        type: str
    state:
        description: Choice between "present" (creates an SSH key) and "absent" (deletes an SSH key). A key will be created only if one with the same name does not yet exist, and deleted only if one with the name is found.
        required: True
        type: str
    public_key:
        description: Body of the SSH public key, defaults to an empty string.
        required: False
        type: str


author:
    - Ana Custura (@ana-cc)
'''

EXAMPLES = r'''
# Pass in a message
- name: Create an SSH key
  greenhost_ssh:
    name: "test-key"
    state: present
    public_key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQChdcj+GLhTme17lt8hKRSx5T5jrJlPlWsNSSK+Dnh594jLea0i/soYT+GimvGqrlZOOknSLDWHlnePa8K9nYm5hElDodmXbTCq2X4YyZ7OgMBKORSmuu5PD9d7MECDPhprPi1p6v6vk2BCjjL2QjOsxYeu5Dj89qcX009oIx== user@host"
- name: Delete an SSH key
    greenhost_ssh:
      name: "test-key"
      state: "absent"
'''

RETURN = r'''
error_message:
    description: The output of any error messages.
    type: str
    returned: always
    sample: "The format of this key is invalid, your SSH key should contain a type and base64 encoded key"
message:
    description: The output message that the module generates.
    type: str
    returned: always
    sample: "SSH key created successfully"
'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(name=dict(type='str', required=True),
                       public_key=dict(type='str', required=False, default=""),
                       state=dict(type='str',
                                  choices=['present', 'absent'],
                                  required=True))

    result = dict(changed=False, error_message='', message='')

    module = AnsibleModule(argument_spec=module_args,
                           supports_check_mode=False)

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    try:
        ssh_keys = cosmos.request_api(resource="account/keys")
    except Exception as e:
        result['error_message'] = str(e)
        module.fail_json(msg=str(e), **result)

    if module.params['state'] == "present":
        for k in ssh_keys["ssh_keys"]:
            if k["name"] == module.params['name']:
                result['message'] = 'Key already exists'
                module.exit_json(**result)
        try:
            # the api raises an error if any of the parameters are wrong
            key_created = cosmos.request_api(resource="account/keys",
                                             request_type="POST",
                                             data={
                                                 "name":
                                                 module.params['name'],
                                                 "public_key":
                                                 module.params['public_key']
                                             })
        except Exception as e:
            result['error_message'] = str(e)
            module.fail_json(msg='The SSH key was not created', **result)

        result[
            'message'] = f"Key with name {module.params['name']} created successfully."
        result['changed'] = True
    else:
        exists = False
        for k in ssh_keys["ssh_keys"]:
            if k["name"] == module.params['name']:
                key_id = k["id"]
                exists = True
        if exists:
            try:
                key_deleted = cosmos.request_api(
                    resource=f"account/keys/{key_id}", request_type="DELETE")
            except:
                result['error_message'] = key_deleted
                module.fail_json(msg='The key was not deleted', **result)
            result[
                'message'] = f"Key(s) with name {module.params['name']} deleted successfully"
            result['changed'] = True
        else:
            result['message'] = 'Key does not exist, nothing to delete'
            module.exit_json(**result)
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
