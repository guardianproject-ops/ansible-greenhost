#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type
import json
from greenhost_cloud import cosmos

DOCUMENTATION = r'''
---
module: greenhost_vm

short_description: Automate the creation and deletion of VMs in Greenhost

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This module relies on the greenhost cloud api to create and delete 
VMs for automation purposes.
The API token must be exported in the environment variable COSMOS_API_TOKEN

options:
    name:
        description: This is the name of the VM to create or terminate
        required: true
        type: str
    state:
        description: Choice between "present" (creates a VM) and "absent" (deletes a VM). A VM will be created if it does not yet exist, and terminated only if it exists.
        required: True
        type: str
    disk:
        description: Disk size of a new VM, in GB. Defaults to 20.
        required: False
        type: int
    ram:
        description: RAM size of a new VM, in MB. Defaults to 2048.
        required: False
        type: int
    ssh_key:
        description: SSH key name to use with a new VM, must already be present in the Greenhost account.
        required: True
        type: str
    region:
        description: Region where a new VM will be created. Defaults to ams1, Amsterdam.
        required: False
        type: str
    image:
        description: image to be used with the new VM. The name must match one of the images present in Greenhost. Defaults to bullseye-x64, Debian Bullseye.
        required: False
        type: str


author:
    - Ana Custura (@ana-cc)
'''

EXAMPLES = r'''
# Pass in a message
- name: Create a VM 
  greenhost_vm:
    name: "test-vm"
    ssh_key: "test-key"
    state: "present"
    disk: 10
    ram: 1024
    image: bullseye-x64

- name: Delete a VM
    greenhost_vm:
      name: "test-vm"
      ssh_key: "test-key"
      state: "absent"
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
error_message:
    description: The output of any error messages.
    type: str
    returned: always
    sample: 'SSH key not found'
message:
    description: The output message that the module generates.
    type: str
    returned: always
    sample: 'VM created successfully'
'''

from ansible.module_utils.basic import AnsibleModule


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(name=dict(type='str', required=True),
                       ssh_key=dict(type='str', required=True),
                       region=dict(type='str', required=False, default='ams1'),
                       ram=dict(type='int', required=False, default=2048),
                       disk=dict(type='int', required=False, default=20),
                       image=dict(type='str',
                                  required=False,
                                  default='bullseye-x64'),
                       state=dict(type='str',
                                  choices=['present', 'absent'],
                                  required=True))

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(changed=False, error_message='', message='')

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(argument_spec=module_args,
                           supports_check_mode=False)

    # manipulate or modify the state as needed (this is going to be the
    # part where your module will do what it needs to do)
    try:
        name_matches = cosmos.get_droplets_by_name(module.params['name'])
        ssh_keys = cosmos.request_api(resource="account/keys")
        images = cosmos.request_api(resource="images")
    except Exception as e:
        result['error_message'] = str(e)
        module.fail_json(msg=str(e), **result)

    for k in ssh_keys["ssh_keys"]:
        if k["name"] == module.params['ssh_key']:
            ssh_key_id = k["id"]
            break

    for i in images["images"]:
        if i["name"] == module.params['image']:
            image_id = i["id"]
            break

    if module.params['state'] == "present":
        for match in name_matches:
            if module.params['name'] == match["name"]:
                result['message'] = 'Droplet already exists'
                module.exit_json(**result)
        try:
            # the api raises an error if any of the parameters are wrong
            droplet_created = cosmos.create_droplet(
                name=module.params['name'],
                ssh_key_id=ssh_key_id,
                region=module.params['region'],
                size=module.params['ram'],
                disk=module.params['disk'],
                image=image_id)
        except Exception as e:
            result['error_message'] = str(e)
            module.fail_json(msg='The droplet was not created', **result)

        result[
            'message'] = f"Droplet with name {module.params['name']} created successfully."
        result['changed'] = True
    else:
        if not name_matches:
            result['message'] = 'Droplet does not exist, nothing to terminate'
            module.exit_json(**result)
        else:
            for match in name_matches:
                if module.params['name'] == match['name']:
                    droplet_deleted = cosmos.terminate_droplet(
                        droplet_id=match["id"])

            name_matches = cosmos.get_droplets_by_name(module.params['name'])
            if not name_matches:
                result[
                    'message'] = f"Droplet(s) with name {module.params['name']} terminated successfully"
                result['changed'] = True
            else:
                result['error_message'] = droplet_deleted
                module.fail_json(msg='The droplet was not deleted', **result)

    # use whatever logic you need to determine whether or not this module
    # made any modifications to your target

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
